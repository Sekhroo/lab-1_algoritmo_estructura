#include <iostream>
using namespace std;

/* Clase: Contiene el contador y el método esencial */
class calculo{

    private:
        /* Atributos */
        int cnt = 0;

    public:
        /* Sumatoria del cuadrado del número con el contador */
        void cnt_sumatoria_cuadrado(int num){
            cnt = cnt + (num * num);
        }

        /* Función: Imprime el contador resultante */
        void get_resultado(){
            cout << "El resultado de la sumatoria de los cuadrados de sus números es: " << cnt;  
        }
};

/* Función: Pregunta al usuario el valor de los N números */
int pregunta_usuario_N(){
    int N;

    cout << "Señale la cantidad de números que ingresará: ";    
    cin >> N;

    return N;
}

/* Función: Pregunta al usuario los números */
int pregunta_usuario_num(){
    int num;

    cout << "Número: ";    
    cin >> num;

    return num;
}

int main() {
    calculo cal = calculo();

    int N = pregunta_usuario_N();
    int arreglo[N];

    
    for (int i = 0; i < N; i++){
        arreglo[i] = pregunta_usuario_num();
        cal.cnt_sumatoria_cuadrado(arreglo[i]);
    }
    cal.get_resultado();
   
    return 0;
}