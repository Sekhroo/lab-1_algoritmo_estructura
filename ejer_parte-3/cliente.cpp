#include <iostream>
using namespace std;

#include "cliente.h"

/* Constructores */
cliente::cliente() {
    string nombre = "\0";
    string telefono = "\0";
    int saldo = 0;
    bool mora = false;
}

cliente::cliente (string nombre, string telefono, int saldo, bool mora) { 
    this->nombre = nombre;
    this->telefono = telefono;
    this->saldo = saldo;
    this->mora = mora;
}

/* Métodos */
/* Función: Imprimir atributos del objeto cliente */
void cliente::data_cliente() {
    cout << "Datos: " << endl << endl;

    cout << "Nombre: " << nombre << endl;
    cout << "Teléfono: " << telefono << endl;
    cout << "Saldo: " << saldo << endl;
    if (mora){
        cout << "Mora: Sí " << endl;
    }else{
        cout << "Mora: No" << endl << endl;
    }
}