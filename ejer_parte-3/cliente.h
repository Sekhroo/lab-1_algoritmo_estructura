#ifndef CLIENTE_H
#define CLIENTE_H

#include <iostream>
using namespace std;

class cliente {
    private:
        /* Atributos */
        string nombre = "\0";
        string telefono = "\0";
        int saldo = 0;
        bool mora = false;
        
    public:
        /* Constructores */
        cliente();
        cliente(string nombre, string telefono, int saldo, bool mora);
        
        /* Métodos */
        void data_cliente();
};
#endif