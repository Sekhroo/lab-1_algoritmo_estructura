#include <iostream>
using namespace std;

#include "cliente.h"

/* Función: Preguntar al usuario el valor de N términos */
int pregunta_usuario_N(){
    int N;

    cout << "Señale la cantidad de caracteres que ingresará: ";    
    cin >> N;

    return N;
}

/* Función: Preguntar el nombre del cliente */
string pregunta_usuario_nombre(){
    string nombre_cliente;

    cout << "Nombre: ";    
    cin >> nombre_cliente;

    return nombre_cliente;
}

/* Función: Preguntar el teléfono del cliente */
string pregunta_usuario_telefono(){
    string telefono_cliente;

    cout << "Teléfono: ";    
    cin >> telefono_cliente;

    return telefono_cliente;
}

/* Función: Preguntar al usuario el saldo del cliente */
int pregunta_usuario_saldo(){
    int saldo_cliente;

    cout << "Saldo: ";    
    cin >> saldo_cliente;

    return saldo_cliente;
}

/* Función: Preguntar al usuario si el cliente es moroso */
bool pregunta_usuario_mora(){
    bool mora_cliente;

    cout << "Mora (true/false): ";    
    cin >> mora_cliente;

    return mora_cliente;
}

int main() {
    string nombre = "\0";
    string telefono = "\0";
    int saldo = 0;
    bool mora = false;

    int N = pregunta_usuario_N();
    cliente arreglo[N];

    /* Función: Creación de los N clientes */
    for (int i = 0; i < N; i++){
        nombre = pregunta_usuario_nombre();
        telefono = pregunta_usuario_telefono();
        saldo = pregunta_usuario_saldo();
        mora = pregunta_usuario_mora();

        cout << endl;    

        arreglo[i] = cliente(nombre, telefono, saldo, mora);
    }

    /* Función: Imprimir datos de los N clientes */
    for (int i = 0; i < N; i++){
        arreglo[i].data_cliente();
    }
 
    return 0;
}