#include <ctype.h>
#include <iostream>
using namespace std;

/* Clase: Contiene contadores y métodos esenciales  */
class calculo{

    private:
        /* Atributos */
        int cnt_mayus = 0;
        int cnt_minus = 0;

    public:
        /* Contadores */
        void contador_mayus(){
            cnt_mayus++;
        }
        void contador_minus(){
            cnt_minus++;
        }

        /* Métodos */
        /* Función: Cuenta las mayúsculas y minúsculas*/
        void analizar_arreglo(char caracter){
            if (isupper(caracter)){
                contador_mayus();
            }else if (islower(caracter)){
                contador_minus();
            }
        }

        /* Función: Imprime los contadores resultantes */
        void get_resultado(){
            cout << "La cantidad de mayusculas digitadas son: " << cnt_mayus << endl;
            cout << "La cantidad de minusculas digitadas son: " << cnt_minus << endl;
        }
};

/* Función: Pregunta al usuario el valor de los N caracteres */
int pregunta_usuario_N(){
    int N;

    cout << "Señale la cantidad de caracteres que ingresará: ";    
    cin >> N;

    return N;
}

/* Función: Pregunta al usuario los caracteres */
char pregunta_usuario_carac(){
    char character;

    cout << "Carácter: ";    
    cin >> character;

    return character;
}

int main() {
    calculo cal = calculo();

    int N = pregunta_usuario_N();
    char arreglo[N];

    for (int i = 0; i < N; i++){
        arreglo[i] = pregunta_usuario_carac();
        cal.analizar_arreglo(arreglo[i]);
    }
    cal.get_resultado();
   
    return 0;
}